import socket
import sys
import dns.resolver
import dns.query
import dns.zone

def zone_query(ip,host):
    print(" IP: %s  HOSTNAME: %s" % (ip,host))
    try:
        z = dns.zone.from_xfr(dns.query.xfr(str(ip), str(host)))
        names = z.nodes.keys()
        names.sort()
        for n in names:
            print(z[n].to_text(n))
    except Exception as e:
        print("errors zone")
        print(e)

#convert ip to hostname
def resolve_ip_to_host(t_ip, dns_type):
    hostname = socket.getfqdn(t_ip)
    host_by_ipaddr = socket.gethostbyaddr(t_ip)
    print(' Querying: %s' % (host_by_ipaddr[0]))
    try:
        get_answers(hostname,dns_type)
    except Exception as e:
        print("SOA Errors: ")
        print(e)

#Convert HOSTNAME to IPADDR
def host_to_ip(hostname):
    dns_ip = socket.gethostbyname(hostname)
    return dns_ip

def get_soa_answers(hostname,dns_type):
    answers = dns.resolver.query(str(hostname), str(dns_type))
    dns_hostname = str(answers[0].mname).rsplit('.',1)[0]
    use_server = dns.resolver.Resolver(configure=False)
    dns_ip = host_to_ip(dns_hostname)
    use_server.nameservers = [str(dns_ip)]
    # zone_query(dns_ip,hostname) # GETTING ERRORS ON ZONE TRANSFER
    #Now we can use their dns server to do resolution
    authoritive_answer = use_server.query(str(hostname), str(dns_type))
    for rdata in authoritive_answer:
        #FOR SOA OBJECTS
        print(' serial: %s  tech: %s' % (rdata.serial, rdata.rname))
        print(' refresh: %s  retry: %s' % (rdata.refresh, rdata.retry))
        print(' expire: %s  minimum: %s' % (rdata.expire, rdata.minimum))
        print(' mname: %s' % (rdata.mname))

def get_mx_answers(hostname,dns_type):
    answers = dns.resolver.query(str(hostname), 'MX')
    for rdata in answers:
        print(' Host', rdata.exchange, 'has preference', rdata.preference)

#NSLOOKUP alike answers
def get_answers(hostname, dns_type): # type being SOA, MX etc...
    #host_by_ipaddr = socket.gethostbyaddr(hostname)
    if dns_type.lower() == 'soa':
        get_soa_answers(hostname,dns_type)
    elif dns_type.lower() == 'mx':
        get_mx_answers(hostname,dns_type)
    elif dns_type.lower() == 'all':
        get_soa_answers(hostname,"SOA")
        get_mx_answers(hostname,"MX")
    else:
        print(" Nothing to do!")

def main(argv):
    # Clear the screen
    #subprocess.call('clear', shell=True)
    resolve_ip_to_host(argv[0], argv[1])
    #check_port(argv[0])
    #netcat(host="localhost", port=6666, content="ls")


if __name__ == "__main__":
    main(sys.argv[1:])
