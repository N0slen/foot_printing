#!/usr/bin/python3
import socket
import sys
import os
import subprocess
import time
import dns.resolver
#from DNSlookup import get_answers


# def netcat(host, port, content):
#     s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#     s.connect_ex((host, int(port)))
#     s.sendall(content.encode())
#     while True:
#         data = s.recv(4096)
#         if not data:
#             break
#         print(repr(data))
#     s.close()

# pass ip(list) and ports(default=none will scan a range of ports) trough args
def check_port(t_ip):
    default_target_ports = [139, 445, 21, 22, 80, 8080, 53, 25, 2525, 587, 4]
    #sock_udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    target_ip = t_ip

    #host_addr = socket.gethostbyaddr(target_ip)
    # get_target_host = socket.getfqdn(host_addr)
    # Try catch
    # print(host_addr)

    #machine_hostname = socket.gethostname()
    #print("HOSTNAME: %s" % str(machine_hostname))
    for each_port in default_target_ports:
        time.sleep(5)
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        result = sock.connect_ex((target_ip, each_port))
        #result_udp = sock_udp.connect_ex((target_ip, each_port))

        if result == 0: # or result_udp == 0:

            host, port = socket.getnameinfo((target_ip, int(each_port)), socket.NI_NUMERICHOST)
            # machine_addrinfo = socket.getaddrinfo()
            try:
                print("STATUS: OPEN"+"   Port_Name: %s" % port)
                # print(machine_addrinfo)
            except socket.gaierror as error:
                print(error)
        else:
            host, port = socket.getnameinfo((target_ip, int(each_port)), socket.NI_NUMERICHOST)
            # machine_addrinfo = socket.getaddrinfo()
            try:
                print("STATUS: CLOSED"+"   Port_Name: %s" % port)
                # print(machine_addrinfo)
            except socket.gaierror as error:
                print(error)

def main(argv):
    # Clear the screen
    subprocess.call('clear', shell=True)
    check_port(argv[0])
    #netcat(host="localhost", port=6666, content="ls")


if __name__ == "__main__":
    main(sys.argv[1:])
